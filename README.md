# Vte3 Bindings

These bindings are based off of the work done by [github user antoyo](https://github.com/antoyo), but I've updated them
to use the latest version of gtk-rs. I won't add them to `crates.io` just because I don't really intend anyone other than
me to use them. If antoyo does update his bindings, I will switch back to them. If you wish to use them in your project
then all you need to add to you `Cargo.toml` is:

```toml
[dependencies.vte]
git = "https://gitlab.com/Miridyan/vte-rs.git"
branch = "master"

[dependencies.vte-sys]
git = "https://gitlab.com/Miridyan/vte-rs.git"
branch = "master"
```
